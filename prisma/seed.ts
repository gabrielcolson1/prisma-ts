import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main(): Promise<void> {
  const user1 = await prisma.users.create({
    data: {
      email: 'alice@prisma.io',
      password: '$2b$10$ZjONRZAxqX2pLoPax2xdcuzABTUEsFanQI6yBYCRtzpRiU4/X1uIu', // "graphql"
    },
  });

  const user2 = await prisma.users.create({
    data: {
      email: 'bob@prisma.io',
      password: '$2b$10$o6KioO.taArzboM44Ig85O3ZFZYZpR3XD7mI8T29eP4znU/.xyJbW', // "secret43"
    },
  });

  // eslint-disable-next-line no-console
  console.log({ user1, user2 });
}

main().finally(async () => {
  await prisma.disconnect();
});
