import { shield } from 'graphql-shield';
import { AuthenticationError } from 'apollo-server-express';

import { isAuthenticated } from './users';

export const permissions = shield({
  Query: {
    me: isAuthenticated,
  },
  Mutation: {
    logout: isAuthenticated,
  },
}, {
  fallbackError: new AuthenticationError('Not Authorised!'),
});
