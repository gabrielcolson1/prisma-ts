import { rule } from 'graphql-shield';

const isAuthenticated = rule()(async (parent, args, ctx) => {
  return Boolean(ctx.userId);
});

export { isAuthenticated };
