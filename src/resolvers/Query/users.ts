import { QueryDefinition, QueryResolver } from '../../types/Resolver';

const me: QueryResolver<'me'> = async (parent, args, ctx) => {
  return ctx.prisma.user.findOne({
    where: {
      id: ctx.userId,
    },
  });
};

const users: QueryDefinition = (t) => {
  t.field('me', {
    type: 'User',
    nullable: true,
    resolve: me,
  });
};

export default users;
