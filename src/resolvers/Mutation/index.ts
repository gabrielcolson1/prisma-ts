import { mutationType } from 'nexus';

import auth from './auth';

const Mutation = mutationType({
  definition(t) {
    auth(t);
  },
});

export { Mutation };
