import { User } from '@prisma/client';
import { compare } from 'bcryptjs';
import { ForbiddenError } from 'apollo-server-express';

import { Context } from '../context';

async function validateUser(email: string, password: string, ctx: Context): Promise<User> {
  const user = await ctx.prisma.user.findOne({
    where: {
      email,
    },
  });

  if (!user) {
    throw new ForbiddenError('Invalid credentials');
  }

  const passwordValid = await compare(password, user.password);

  if (!passwordValid) {
    throw new ForbiddenError('Invalid credentials');
  }
  return user;
}

export { validateUser };
